import urllib, json, time, sys, os, urllib2, base64


buildType = sys.argv[1]
buildVersion = int(sys.argv[2])
host = sys.argv[3]
webapps_arg = ''
if len(sys.argv) > 4:
    webapps_arg = sys.argv[4]
print("********************************************************")
print("***")
print("***                Deploying on "+ host)
print("***")
print("********************************************************")

time.sleep(30)

webapps_path = '/home/ec2-user/kazi/env/tomcat/apache-tomcat-7.0.88/webapps/'

if len(webapps_arg) > 0:
    webapps_path = webapps_arg

job_name = 'kazi-dev-build'
if buildType == 'dev':
    job_name = 'kazi-dev-build'
elif buildType == 'prod':
    job_name = 'kazi-prod-build'

oldbuild = buildVersion - 1
jenkins_host = 'jenkins.glpapps.com'
build_url = 'http://'+ jenkins_host +'/job/'+job_name+'/lastStableBuild/buildNumber'
try:
    request = urllib2.Request(build_url)
    base64string = base64.b64encode('%s:%s' % ('admin', 'glp@123'))
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request)
    oldbuild = result.read()
except Exception:
    print('cant read lastsuccessful build. will use previous buildVersion as old build')

print('old build is %s' % str(oldbuild))
print('new build is %s' % str(buildVersion))

url = 'http://'+host+':8080/kazi/kazi-core/v1.0/version'

def print_exit():
    print("########################################################")
    print("                   Exit from " + host)
    print("########################################################")

try:
    result = json.load(urllib.urlopen(url))
    try:
        if result['ResponseData']['buildVersion'] == buildVersion:
            print('New war is deployed and Version is correct')
            print('backing previous war kazi##%d.war ->->-> kazi##%d.war.bak' % (int(oldbuild), int(oldbuild)))
            mv_command = "mv /home/ec2-user/kazi/env/tomcat/apache-tomcat-7.0.88/webapps/kazi##%d.war /home/ec2-user/kazi/env/tomcat/apache-tomcat-7.0.88/webapps/kazi##%d.war.bak" % (int(oldbuild), int(oldbuild))
            print mv_command
            os.system(mv_command)
            print_exit()
            exit(0)
        elif result['ResponseData']['buildVersion'] > buildVersion:
            pass
        else:
            print('New war is deployed but version is not correct: reverting back')
            mv_command = "mv /home/ec2-user/kazi/env/tomcat/apache-tomcat-7.0.88/webapps/kazi##%d.war /home/ec2-user/kazi/env/tomcat/apache-tomcat-7.0.88/webapps/kazi##%d.war.bak" % (buildVersion, buildVersion)
            print mv_command
            os.system(mv_command)
            print_exit()
            exit(1)
    except KeyError:
        print('buildVersion not found.')
        print(result)
        print_exit()
        exit(1)
except Exception:
    print('Unable to connect version api.')
    print_exit()
    exit(1)
