import urllib, json, time, sys, os, urllib2, base64

jenkins_host = 'jenkins.glpapps.com'
host = ''
if len(sys.argv) > 1:
    host = sys.argv[1]
print('revert_back on '+ host)

def get_data_from_jenkins(url):
    try:
        request = urllib2.Request(url)
        base64string = base64.b64encode('%s:%s' % ('admin', 'glp@123'))
        request.add_header("Authorization", "Basic %s" % base64string)
        result = urllib2.urlopen(request)
        return result.read()
    except Exception:
        print('cant read lastBuild build. on ' + host)
        print('Its FATAL.FATAL.FATAL. on ' + host)
        exit(1)

lastBuildUrl = 'http://' + jenkins_host + '/job/kazi-prod-build/lastBuild/api/json'
response_json = json.loads(get_data_from_jenkins(lastBuildUrl))
success = response_json['result']

if success == 'SUCCESS':
    print('Build was successful nothing will be reverted on '+host)
    exit(0)
print("********************************************************")
print("***                Reverting on "+ host)
print("********************************************************")
webapps_arg = ''
if len(sys.argv) > 2:
    webapps_arg = sys.argv[2]

webapps_path = '/home/ec2-user/kazi/env/tomcat/apache-tomcat-7.0.88/webapps/'

if len(webapps_arg) > 0:
    webapps_path = webapps_arg

job_name = 'kazi-prod-build'
buildVersion = None
lastBuildUrl = 'http://'+jenkins_host+'/job/'+job_name+'/lastBuild/buildNumber'
try:
    request = urllib2.Request(lastBuildUrl)
    base64string = base64.b64encode('%s:%s' % ('admin', 'glp@123'))
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request)
    buildVersion = result.read()
except Exception:
    print('cant read lastBuild build. on ' + host)
    print('Its FATAL.FATAL.FATAL. on ' + host)
    print("#####->->->->->->->->->->->->->->->->->->->->->->->->->->->")
    print("###               Reverting on " + host)
    print("#####->->->->->->->->->->->->->->->->->->->->->->->->->->->")
    exit(1)


buildVersion = int(buildVersion)
oldbuild = buildVersion - 1
build_url = 'http://'+ jenkins_host +'/job/'+job_name+'/lastStableBuild/buildNumber'
try:
    request = urllib2.Request(build_url)
    base64string = base64.b64encode('%s:%s' % ('admin', 'glp@123'))
    request.add_header("Authorization", "Basic %s" % base64string)
    result = urllib2.urlopen(request)
    oldbuild = result.read()
except Exception:
    print('cant read lastsuccessful build. will use previous buildVersion as old build on ' + host)

print('old build is %s on %s' % (str(oldbuild), host))
print('new build is %s on %s' % (str(buildVersion), host))

try:
    print('****Reverting war to lastSuccessful Version**** on '+host)
    print('War was not deployed correctly on some server on '+host)
    mv_command = "mv " + webapps_path + "kazi##%d.war.bak " + webapps_path + "kazi##%d.war"
    mv_command = mv_command % (int(oldbuild), int(oldbuild))
    print(mv_command)
    os.system(mv_command)
    mv_command = "mv "+ webapps_path +"kazi##%d.war "+ webapps_path +"kazi##%d.war.bak"
    mv_command = mv_command % (buildVersion, buildVersion)
    print(mv_command)
    os.system(mv_command)
except Exception as e:
    print('Some error ecored during fallback. on '+ host)
    print(e)
    print("#####->->->->->->->->->->->->->->->->->->->->->->->->->->->")
    print("###               Reverting on " + host)
    print("#####->->->->->->->->->->->->->->->->->->->->->->->->->->->")
    exit(1)
